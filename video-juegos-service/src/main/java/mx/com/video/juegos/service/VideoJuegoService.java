package mx.com.video.juegos.service;

import java.util.List;

import mx.com.video.juegos.model.VideoJuegoModel;

public interface VideoJuegoService {

	List<VideoJuegoModel> getDestacados();
	List<VideoJuegoModel> getDistribuidor(int id_distribuidor);
	List<VideoJuegoModel> getGamesForName(String name);
	List<VideoJuegoModel> findByNombreContaining(String name);
	VideoJuegoModel save(VideoJuegoModel v);

}
