package mx.com.video.juegos.service;

import java.util.List;

import mx.com.video.juegos.model.DistribuidorModel;

public interface DistribuidorService {

	List<DistribuidorModel> getAll();
}
