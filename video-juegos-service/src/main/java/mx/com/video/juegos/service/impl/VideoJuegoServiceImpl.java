package mx.com.video.juegos.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mx.com.video.juegos.dao.VideoJuegosDao;
import mx.com.video.juegos.model.VideoJuegoModel;
import mx.com.video.juegos.service.VideoJuegoService;

@Service
public class VideoJuegoServiceImpl implements VideoJuegoService{

	@Autowired
	private VideoJuegosDao videoJuegosDao;

	@Override
	public List<VideoJuegoModel> getDestacados() {
		return videoJuegosDao.getAllGames();
	}

	@Override
	public List<VideoJuegoModel> getDistribuidor(int id_distribuidor) {
		return videoJuegosDao.getDistribuidor(id_distribuidor);
	}

	@Override
	public List<VideoJuegoModel> getGamesForName(String name) {
		return videoJuegosDao.getGamesForName(name);
	}

	@Override
	public List<VideoJuegoModel> findByNombreContaining(String name) {
		return videoJuegosDao.findByNombreContaining(name);
	}

	@Override
	public VideoJuegoModel save(VideoJuegoModel v) {
		return videoJuegosDao.save(v);
	}
	

}
