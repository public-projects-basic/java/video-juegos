package mx.com.video.juegos.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mx.com.video.juegos.dao.DistribuidorDao;
import mx.com.video.juegos.model.DistribuidorModel;
import mx.com.video.juegos.model.VideoJuegoModel;
import mx.com.video.juegos.service.DistribuidorService;

@Service
public class DistribuidorServiceImpl implements DistribuidorService{

	@Autowired
	private DistribuidorDao distribuidorDao;
	
	@Override
	public List<DistribuidorModel> getAll() {
		return distribuidorDao.findAll();
	}

}
