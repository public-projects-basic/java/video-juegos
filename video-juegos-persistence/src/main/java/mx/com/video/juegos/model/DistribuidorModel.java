package mx.com.video.juegos.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "distribuidor", schema = "test")
public class DistribuidorModel {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_distribuidor")
	private int id;
	@Column(name = "nombre")
	private String nombre;
	@Column(name = "url")
	private String url;

	public int getId() { return id; }
	public String getNombre() { return nombre; }
	public String getUrl() { return url; }
	
	public void setId(int id) { this.id = id; }
	public void setNombre(String nombre) { this.nombre = nombre; }
	public void setUrl(String url) { this.url = url; }

	@Override
	public String toString() {
		return "DistribuidorModel [id=" + id + ", nombre=" + nombre + ", url=" + url + "]";
	}
	
}
