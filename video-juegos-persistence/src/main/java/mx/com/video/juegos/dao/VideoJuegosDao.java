package mx.com.video.juegos.dao;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import mx.com.video.juegos.model.VideoJuegoModel;

public interface VideoJuegosDao extends JpaRepository<VideoJuegoModel, Integer>{

	@Query("from VideoJuegoModel v order by v.nombre")
	List<VideoJuegoModel> getAllGames();
	
	@Query("from VideoJuegoModel v WHERE v.distribuidor.id = ?1 order by v.nombre")
	List<VideoJuegoModel> getDistribuidor(int id_distribuidor);
	
	@Query("from VideoJuegoModel v WHERE v.nombre like %?1% order by v.nombre")
	List<VideoJuegoModel> getGamesForName(String name);

	List<VideoJuegoModel> findByNombreContaining(String name);

	//	UTILIZAR QUERY NATIVOS DE SQL
	//	@Query(value = "SELECT * FROM video_juegos v order by v.nombre", nativeQuery = true)
	//	List<VideoJuegoModel> getAllGames();
	
}
