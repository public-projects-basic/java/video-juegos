package mx.com.video.juegos.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name ="video_juegos", schema = "test")
public class VideoJuegoModel {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_video_juego")
	private int id;
	@Column(name = "nombre")
	private String nombre;
	@Column(name = "descripcion")
	private String descripcion;
	@Column(name = "imagen")
	private String imagen;
	
	@ManyToOne
	@JoinColumn(name = "id_distribuidor")
	private DistribuidorModel distribuidor;
	
	public int getId() { return id; }
	public String getNombre() { return nombre; }
	public String getDescripcion() { return descripcion; }
	public String getImagen() { return imagen; }
	public DistribuidorModel getDistribuidor() { return distribuidor; }

	public void setId(int id) { this.id = id; }
	public void setNombre(String nombre) { this.nombre = nombre; }
	public void setDescripcion(String descripcion) { this.descripcion = descripcion; }
	public void setImagen(String imagen) { this.imagen = imagen; }
	public void setDistribuidor(DistribuidorModel distribuidor) { this.distribuidor = distribuidor; }

	@Override
	public String toString() {
		return "VideoJuegoModel [id=" + id + ", nombre=" + nombre + ", descripcion=" + descripcion + ", imagen="
				+ imagen + ", distribuidor=" + distribuidor + "]";
	}
	
}
