package mx.com.video.juegos.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import mx.com.video.juegos.model.DistribuidorModel;

public interface DistribuidorDao extends JpaRepository<DistribuidorModel, Integer>{

	
}
