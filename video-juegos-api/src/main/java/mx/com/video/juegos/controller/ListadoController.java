package mx.com.video.juegos.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import mx.com.video.juegos.model.VideoJuegoModel;
import mx.com.video.juegos.service.VideoJuegoService;

@Controller
@RequestMapping("/")
public class ListadoController {

	@Autowired
	private VideoJuegoService videoJuegoService;

	@GetMapping("/")
	private String getVideoJuegos(Model model) {
		List<VideoJuegoModel> lst = videoJuegoService.getDestacados();
		model.addAttribute("destacados", lst);
		return "listado";
	}
	
	@GetMapping("/videoJuegosDistribuidor")
	private String getDistribuidor(int distribuidor, Model model) {
		List<VideoJuegoModel> lst = videoJuegoService.getDistribuidor(distribuidor);
		model.addAttribute("destacados", lst);
		return "listado";
	}
	
	@GetMapping("/buscar")
	private String getDistribuidor(@RequestParam("q") String juego, Model model) {
		List<VideoJuegoModel> lst = videoJuegoService.findByNombreContaining(juego);
		model.addAttribute("destacados", lst);
		return "listado";
	}
	

}
