package mx.com.video.juegos.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import mx.com.video.juegos.model.VideoJuegoModel;
import mx.com.video.juegos.service.DistribuidorService;
import mx.com.video.juegos.service.VideoJuegoService;

@Controller
@RequestMapping("/")
public class VideoJuegosController {

	@Autowired
	private DistribuidorService distribuidorService;
	
	@Autowired
	private VideoJuegoService videoJuegoService;

	@GetMapping("/crear")
	public String getFormulario(Model model) {		
		model.addAttribute("distribuidores", distribuidorService.getAll());
		model.addAttribute("videoJuego", new VideoJuegoModel());
		return "formVideoJuego";
	}
	
	@PostMapping("/guardar")
	public String saveGame(VideoJuegoModel videoJuego) {
		videoJuegoService.save(videoJuego);
		return "redirect:/";
	}
	
}
