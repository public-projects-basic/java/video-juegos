CREATE DATABASE test;
CREATE USER admin@'%' IDENTIFIED BY 'admin';
GRANT ALL PRIVILEGES ON *.* TO admin@'%';
set NAMES utf8;
CREATE TABLE test.video_juegos(
    id_video_juego INT NOT NULL AUTO_INCREMENT,
    nombre VARCHAR(100) NOT NULL,
    descripcion VARCHAR(2000),
    imagen VARCHAR(2000),
    CONSTRAINT pk001video_juegos  PRIMARY KEY (id_video_juego),
    CONSTRAINT uq001video_juegos UNIQUE (nombre),
    CONSTRAINT uq002video_juegos UNIQUE (imagen)
)ENGINE = INNODB;
INSERT INTO test.video_juegos (nombre, descripcion, imagen) VALUES 
    ('Contra', 
    'Contra es una serie de videojuegos creada por la compañía japonesa Konami en 1987, cuyos primeros títulos recibieron el nombre de Probotector en Europa. Son fundamentalmente shoot \'em ups de tipo run and gun y están inspirados en películas como Rambo o Alien.', 
    'https://sm.ign.com/ign_es/gallery/c/contra-ann/contra-anniversary-collection-images_qp5f.png'),
    ('The King of Fighters', 
    'The King of Fighters, abreviada KOF, y traducida como El Rey De Los Luchadores, es una saga de videojuegos de lucha inicialmente para el sistema Neo Geo desarrollada por la compañía SNK.', 
    'https://pm1.narvii.com/6910/4ea485cc168fdc44eca53b09331a85f5c05925a7r1-960-710v2_uhq.jpg'),
    ('Street Fighter', 
    'Street Fighter es una serie de videojuegos de lucha creada por la empresa japonesa Capcom. El primer título, Street Fighter, apareció en el año 1987 en Arcade y gozó de cierta popularidad.',
    'https://as.com/meristation/imagenes/2020/08/02/noticias/1596355304_898885_1596355548_noticia_normal.jpg'),
    ('Killer Instinct',
    'Killer Instinct es una serie de videojuegos de lucha creada por el estudio británico Rare, siendo su primer proyecto en este género. Su primer juego fue distribuido por Midway y programado por Rare en 1994.',
    'https://cdn.hobbyconsolas.com/sites/navi.axelspringer.es/public/styles/1200/public/media/image/2013/11/262374-killer-instinct-ultra-tendra-extras-jugosos.jpg?itok=QazoewW-'),
    ('Mortal Kombat',
    'Mortal Kombat es una franquicia de videojuegos de peleas creada por Ed Boon y John Tobias en 1992. Las cuatro primeras entregas fueron distribuidas por Midway Games y lanzadas principalmente en máquinas arcade; posteriormente estuvieron disponibles en consolas domésticas.',
    'https://www.cinemascomics.com/wp-content/uploads/2018/07/mortal-kombat-logo.jpg'),
    ('Samurai Shodown',
    'Samurai Shodown es una franquicia de videojuegos de pelea de SNK. Es una de las sagas más populares de la compañía junto a Fatal Fury, The King of Fighters y Metal Slug. ',
    'https://cdn02.nintendo-europe.com/media/images/10_share_images/games_15/nintendo_switch_4/H2x1_NSwitch_SamuraiShodown.jpg'),
    ('Metal Slug',
    'Metal Slug es una serie de videojuegos de tipo run and gun y shoot \'em up lanzado inicialmente en las máquinas arcade Neo-Geo y en consolas de juegos creadas por SNK',
    'https://larepublica.pe/resizer/rJUNV_JDYXOMowrIp_FUHSg7_QM=/646x380/top/smart/arc-anglerfish-arc2-prod-gruporepublica.s3.amazonaws.com/public/4GWU6Z2SXBEQFDM5UJJS2GPPLQ.png'),
    ('Shock Troopers',
    'Shock Troopers es un juego arcade de carreras y armas desarrollado por Saurus y publicado por SNK en 1997 para la plataforma de juegos y plataformas Neo-Geo. El juego implica tomar el mando de uno o tres soldados en un tirador de ocho vías. Un segundo juego de la serie, Shock Troopers: 2nd Squad, siguió en 1998.',
    'https://steamcdn-a.akamaihd.net/steam/apps/366270/ss_2d9bc745df904142c5afd6447d3a18cfa5e9279e.1920x1080.jpg?t=1572598267'),
    ('Crash Bandicoot',
    'Crash Bandicoot es el nombre de una serie de videojuegos protagonizado por el personaje del mismo nombre. Fue creada en 1996 por Naughty Dog, quien desarrolló los primeros cuatro títulos, bajo la distribución de Universal Interactive Studios.',
    'https://lh3.googleusercontent.com/proxy/KqLoYTy7cCAiATcX3wd0-Z-wWl-hEO0DhrwbMC90PZKWnafzZ3_CfoXdm0iZFMAiQP1MJ_oi46ZC-73s7fMLILcxIY4XThVyE4CW71pzeWTX9oE7Kfm723ndrxuuUaU9NmCZVfCiYLQUDDQ'),
    ('Donkey Kong Country',
    'Donkey Kong Country es un videojuego desarrollado por la compañía Rareware y Nintendo, que incluye la familia del popular personaje de videojuegos, Donkey Kong. Este fue lanzado para la videoconsola Super Nintendo Entertainment System en 1994. El juego fue lanzado en Japón bajo el título Super Donkey Kong.',
    'https://as.com/meristation/imagenes/2019/11/24/noticias/1574584062_344374_1574584406_noticia_normal.jpg'),
    ('Super Mario World',
    'Super Mario World conocido inicialmente en Japón como Super Mario World: Super Mario Bros. 4, es un videojuego de plataformas desarrollado y publicado por Nintendo',
    'https://cdn.hobbyconsolas.com/sites/navi.axelspringer.es/public/styles/480/public/media/image/2017/09/analisis-super-mario-world.jpg?itok=VAN_CeEz');
CREATE TABLE test.distribuidor(
    id_distribuidor INT NOT NULL AUTO_INCREMENT,
    nombre VARCHAR(200) NOT NULL,
    url VARCHAR(1000) NOT NULL,
    CONSTRAINT pk002distribuidor PRIMARY KEY (id_distribuidor),
    CONSTRAINT uq003distribuidor UNIQUE (nombre),
    CONSTRAINT uq004distribuidor UNIQUE (url)
) ENGINE = INNODB;
INSERT INTO test.distribuidor(nombre, url) VALUES
	('SNK', 'https://www.snk-corp.co.jp'),
	('Nintendo', 'https://www.nintendo.com'),
	('Playstation', 'https://www.playstation.com'),
	('Electronic Arts', 'https://www.ea.com/es-mx');
ALTER TABLE test.video_juegos ADD COLUMN id_distribuidor INT;
UPDATE test.video_juegos SET id_distribuidor = 1 WHERE id_video_juego = 1;
UPDATE test.video_juegos SET id_distribuidor = 2 WHERE id_video_juego = 2;
UPDATE test.video_juegos SET id_distribuidor = 3 WHERE id_video_juego = 3;
UPDATE test.video_juegos SET id_distribuidor = 4 WHERE id_video_juego = 4;
UPDATE test.video_juegos SET id_distribuidor = 1 WHERE id_video_juego = 5;
UPDATE test.video_juegos SET id_distribuidor = 2 WHERE id_video_juego = 6;
UPDATE test.video_juegos SET id_distribuidor = 3 WHERE id_video_juego = 7;
UPDATE test.video_juegos SET id_distribuidor = 4 WHERE id_video_juego = 8;
UPDATE test.video_juegos SET id_distribuidor = 1 WHERE id_video_juego = 9;
UPDATE test.video_juegos SET id_distribuidor = 2 WHERE id_video_juego = 10;
UPDATE test.video_juegos SET id_distribuidor = 3 WHERE id_video_juego = 11;
ALTER TABLE test.video_juegos ADD CONSTRAINT fk001video_juegos FOREIGN KEY (id_distribuidor) REFERENCES test.distribuidor(id_distribuidor);